package com.viken.smc;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.viken.motorlib.ShutterControl;
import com.viken.motorlib.ChopperControl;

public class MainActivity extends Activity {

    private android.os.Handler updateHandler;
    private Runnable updateRunnable;
    private int DELAYTIME = 500;
    private ChopperControl cc;
    private ShutterControl sc;
    private TextView chopperStatus;
    private Thread cycleThread;
    private boolean monitorEnabled = false;
    private Button chopperCycle;
    private Button shutterCycle;
    private Thread shutterThread;
    private TextView shutterStatus;

    private String host = "10.150.1.11";
    //private String host = "192.168.1.11";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        cc = new ChopperControl(host);
        sc = new ShutterControl(host);

        TextView ipAddressTextView = findViewById(R.id.ipAddressTextView);
        ipAddressTextView.setText(host);

        // chopper elements
        TextView chopperInfo = findViewById(R.id.chopperInfo);
        chopperStatus = findViewById(R.id.chopperStatus);
        shutterStatus = findViewById(R.id.shutterStatus);
        final com.viken.smc.WrapperSpinner chopperSpeed = findViewById(R.id.chopperSpeed);
        final com.viken.smc.WrapperSpinner chopperCycleCount = findViewById(R.id.chopperCycleCount);
        final com.viken.smc.WrapperSpinner shutterPercentSpinner = findViewById(R.id.shutterPercentSpinner);
        final com.viken.smc.WrapperSpinner shutterCycleCount = findViewById(R.id.shutterCycleCount);
        final Button connect = findViewById(R.id.connect);
        if (cc.isConnected()) {
            connect.setText("Disconnect");
        } else {
            connect.setText("Connect");
        }
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connect.getText().equals("Connect")) {
                    cc.connect();
                    //sc.connect();
                } else {
                    cc.disconnect();
                    //sc.disconnect();
                }
                if (cc.isConnected()) {
                    connect.setText("Disconnect");
                } else {
                    connect.setText("Connect");
                }
            }
        });
        final Button chopperEnable = findViewById(R.id.chopperEnable);
        chopperEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chopperEnable.getText().equals("Enable")) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (cc.enable(Integer.valueOf(chopperSpeed.getSelectedItem().toString()))) {
                                //monitorEnabled = true;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        uiText(chopperEnable, "Disable");
                                        uiText(chopperStatus, chopperSpeed.getSelectedItem().toString());
                                    }
                                });
                            }
                            //if (!actuallyEnabled)
                        }
                    }).start();
                } else {
                    chopperEnable.setText("Enable");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            cc.disable();
                            //monitorEnabled = false;
                        }
                    }).start();
                }
            }
        });

        chopperCycle = findViewById(R.id.chopperCycle);
        chopperCycle.setText("Cycle Enable");
        chopperCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chopperCycle.getText().equals("Cycle Enable")) {
                    uiText(chopperCycle, "Cycle Disable");
                    cycleThread = chopperCycle(Integer.valueOf(chopperCycleCount.getSelectedItem().toString()),
                            Integer.valueOf(chopperSpeed.getSelectedItem().toString()), chopperStatus);
                 } else {
                    uiText(chopperCycle, "Cycle Enable");
                    cycleThread.interrupt();
                 }
            }
        });

        Button chopperTest = findViewById(R.id.chopperTest);
        chopperTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        cc.Test();
                    }
                }).start();
            }
        });

        Button shutterOpen = findViewById(R.id.shutterOpen);
        shutterOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (!sc.Open()) {
                            uiText(shutterStatus, "failed to open shutter");
                            return;
                        }
                        uiText(shutterStatus, "Motion in progress");
                        sc.WaitWhileHoming();
                        uiText(shutterStatus, "Motion done");
                    }
                }).start();
            }
        });
        Button shutterClose = findViewById(R.id.shutterClose);
        shutterClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (!sc.Close()) {
                            uiText(shutterStatus, "failed to close shutter");
                            return;
                        }
                        uiText(shutterStatus, "Motion in progress");
                        sc.WaitWhileHoming();
                        uiText(shutterStatus, "Motion done");
                    }
                }).start();
            }
        });
        Button shutterPercent = findViewById(R.id.shutterPercent);
        shutterPercent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int percent = Integer.valueOf(shutterPercentSpinner.getSelectedItem().toString());
                sc.PercentOpen(percent);
                uiText(shutterStatus, "Motion in progress");
                sc.WaitWhileMoving(percent);
                uiText(shutterStatus, "Motion done");
            }
        });

        shutterCycle = findViewById(R.id.shutterCycle);
        shutterCycle.setText("Cycle Enable");
        shutterCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shutterCycle.getText().equals("Cycle Enable")) {
                    uiText(shutterCycle, "Cycle Disable");
                    shutterThread = shutterCycle(Integer.valueOf(shutterCycleCount.getSelectedItem().toString()), shutterStatus);
                } else {
                    uiText(shutterCycle, "Cycle Enable");
                    shutterThread.interrupt();
                }
            }
        });

        Button shutterTest = findViewById(R.id.shutterTest);
        shutterTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        sc.Test();
                    }
                }).start();
            }
        });


        final TextView serverInfo = (TextView) findViewById(R.id.serverInfo);
        updateHandler = new android.os.Handler();
        updateRunnable = new Runnable() {
            @Override
            public void run() {
                if (monitorEnabled)
                    updateThread(serverInfo);
                updateHandler.postDelayed(updateRunnable, DELAYTIME);
            }
        };

        ToggleButton monitorButton = (ToggleButton) findViewById(R.id.monitorButton);
        monitorButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    monitorEnabled = true;
                } else {
                    monitorEnabled = false;
                }
            }
        });
        updateHandler.postDelayed(updateRunnable, DELAYTIME);
    }

    private void updateThread(TextView tv) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int curr = cc.getCurrent();
                int rpm = cc.getRpm();
                runOnUiThread(() -> {
                    String text = "RPM (avg) " + rpm + "\n" + "Curr (avg) " + curr;
                    tv.setText(text);
                });
            }
        }).start();
    }

    private Thread chopperCycle(int cnt, int rpm, TextView tv) {
        final int delay = 100;
        Thread th = new Thread() {
            int thCnt = 1;
            public void run() {
                for (int i = 0; i < cnt; i++) {
                    uiText(tv, "enable to " + rpm);
                    cc.enable(rpm);
                    while(cc.getRpm() < rpm) {
                        try {Thread.sleep(delay);} catch (InterruptedException e) { return; }
                    }
                    uiText(tv, "disable, cnt " + thCnt++);
                    cc.disable();
                    while(cc.getRpm() > 30) {
                        try {Thread.sleep(delay);} catch (InterruptedException e) { return; }
                    }
                }
                chopperCycle.setText("Cycle Enable");
                uiText(tv, "Done with " + thCnt);
            }
        };
        th.start();
        return th;
    }

    private Thread shutterCycle(int cnt, TextView tv) {
        final int delay = 100;
        Thread th = new Thread() {
            int thCnt = 1;
            public void run() {
                for (int i = 0; i < cnt; i++) {
                    uiText(tv, "Open shutter ");
                    sc.Open();
                    sc.WaitWhileHoming();
                    uiText(tv, "Close shutter, cnt " + thCnt++);
                    sc.Close();
                    sc.WaitWhileHoming();
                }
                uiText(chopperCycle, "Cycle Enable");
                uiText(tv, "Done with " + thCnt);
            }
        };
        th.start();
        return th;
    }

    private void uiText(TextView tv, String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv.setText(s);
            }
        });
    }

}
